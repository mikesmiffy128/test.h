main.o: main.c
	@cc -c $< -o $@

ext.o: ext.c
	@cc -c $< -o $@

myprog-untested: main.o ext.o
	@cc $^ -o $@

main.test: main.test.c
	@cc $< -o $@

ext.test: ext.test.c
	@cc $< -o $@

TESTS=main.test ext.test
myprog: $(TESTS) myprog-untested
	@stat=0; \
	for prg in $(TESTS); do \
		./$$prg || stat=1; \
	done; \
	if [ $$stat != 0 ]; then \
		exit 1; \
	fi \
	cp myprog-untested myprog

